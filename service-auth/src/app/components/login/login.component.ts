import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user = {
    username:'',
    password:''
  };

  
  constructor(private authService:AuthService, private router: Router) { }
  
  ngOnInit(): void {
  }
  
  onloginClicked() {
    const success = this.authService.login(this.user);
    if (success) {
      //redirect to dashboard
      this.router.navigateByUrl('/dashboard');
    }else {
      alert("wrong login")
    }
   
    
  };
}
