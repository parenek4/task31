import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private user: any = null;

  constructor() { }
  /**
   * Attempt to log in with given user
   * @param user 
   */
  public login(user: any): boolean{
    if(user.username == 'peter' &&  user.password == 'secret') {
      this.user = {...user};
    } else {
      this.user = null;
    }

    return this.isLoggedIn();

  }

  public isLoggedIn(): boolean {
    return this.user !== null;

  }

}
